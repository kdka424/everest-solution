
## Everest Solution LLC


## Installation

```bash
    create database
    clone the project 
    cd everest-solution
    composer install
    php artiasn key:generate
    cp .env.example .env
    update .env with database credentials
    npm install 
    npm run dev
    php artisan migrate
    php artisan serve
```
    